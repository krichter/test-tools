/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.test.tools;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import org.apache.commons.lang3.mutable.MutableInt;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author richter
 */
public class ParallelITExecutorTest {

    @Test
    public void testExecuteLambdaInParallel() throws ExecutionException,
            InterruptedException {
        MutableInt result = new MutableInt();
        Callable<Void> parallelITLambda = () -> {
            synchronized(this) {
                result.increment();
            }
            return null;
        };
        int parallelism = 100;
        ParallelITExecutor instance = new ParallelITExecutor();
        instance.executeLambdaInParallel(parallelism,
                parallelITLambda);
        assertEquals(parallelism,
                (long)result.getValue());
    }
}
