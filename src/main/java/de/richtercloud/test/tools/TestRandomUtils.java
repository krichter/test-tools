/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.test.tools;

import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides access to an insecure test random.
 * @author richter
 */
public final class TestRandomUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestRandomUtils.class);
    private final static Random RANDOM;
    static {
        long randomSeed = System.currentTimeMillis();
        LOGGER.info(String.format("random seed: %d", randomSeed));
        RANDOM = new Random(randomSeed);
    }

    /**
     * Provides a {@link Random} whose initial seed has been revealed in a log message which is a severe security risk
     * in a production environment, but makes a lot of sense for use in tests based on random data because it allows
     * randomly failing test cases to be reproduced.
     *
     * @return an insecure {@code Random} which is intented for use in tests
     */
    public static Random getInsecureTestRandom() {
        return RANDOM;
    }

    private TestRandomUtils() {
    }
}
