/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.richtercloud.test.tools;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author richter
 */
public class ParallelITExecutor {
    private final static Logger LOGGER = LoggerFactory.getLogger(ParallelITExecutor.class);

    public void executeLambdaInParallel(int parallelism,
            Callable<Void> parallelITLambda) throws ExecutionException,
            InterruptedException {
        Queue<Future<Void>> futures = new LinkedList<>();
        ExecutorService executorService = Executors.newCachedThreadPool();
        for(int i=0;i<parallelism; i++) {
            Future<Void> future = executorService.submit(parallelITLambda);
            futures.add(future);
        }
        executorService.shutdown();
        while(!futures.isEmpty()) {
            LOGGER.debug(String.format("waiting for %d parallel test runs to complete",
                    futures.size()));
            Future<Void> future = futures.poll();
            future.get();
        }
        //executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
            //shouldn't be necessary
    }
}
